const express = require('express');
const mysql = require('mysql2');



//const dotenv = require('dotenv');
//require('dotenv').config()

const  conn = mysql.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS, 
    database: process.env.DB_DATABASE,
  })

 conn.connect((err) => {
      if(err){
          console.log("ERROR: " + err.message);
          return;    
      }
      console.log('Connection established');
    })

    let dataPool={}

    dataPool.AddUser=(username,password,email,userType)=>{
      return new Promise ((resolve, reject)=>{
        conn.query(`INSERT INTO User (username,password,email,userType) VALUES (?,?,?,?)`, [username, email, password,userType], (err,res)=>{
          if(err){return reject(err)}
          return resolve(res)
        })
      })
    }
    
   // module.exports = conn;
module.exports = dataPool;