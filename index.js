const express = require('express')
const app = express()
const dotenv = require('dotenv');
require('dotenv').config()
const bodyParser = require('body-parser');
const cors=require("cors")
const bcrypt = require('bcrypt');
const conn = require('./database.js');



const port = process.env.PORT || 8090
const users = require('./models/users')
app.use(bodyParser.json());
app.use(cors());

//Some configurations
app.use(express.urlencoded({extended : true}));
app.use(cors({
 methods:["GET", "POST"],
}))

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
//const novice = require('./routes/novice')
//const users = require('./routes/users')

//app.use('/novice', novice)
//app.use('/users', users)


      
app.use('/users', users)




app.get('/', (req, res) => res.send('Hello World!!'))
app.listen(port, () => console.log(`Example app listening on port ${port}!`))